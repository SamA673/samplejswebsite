/**
 * @description 
 * changeTextColor set the color of selector tag 
 * with the color value from inputTextColor HTMLElement 
 * @param {HTMLElement} selector
 * @param {HTMLInputElement} inputTextColor //color input
 */
function changeTextColor(selector, inputTextColor) {
    const element = document.querySelector(selector);
    const color = inputTextColor.value; 
    element.style.color = color;

}


/**
 * @description 
 * changeTagWidth set the width (in %) of HTMLElement tag 
 * with the value from inputTextWidth HTMLElement 
 * @param {HTMLElement} selector 
 * @param {HTMLInputElement} inputTagWidth //width input
 */

function changeTagWidth(selector, inputTagWidth) {
    const element = document.querySelector(selector);
    const width = inputTagWidth.value+"%"; 
    element.style.width = width;
}


/**
 * @description 
 * changeBackground set the background of HTMLElement tag 
 * with the value from inputBgdColor HTMLElement 
 * @param {HTMLElement} selector
 * @param {HTMLInputElement} inputBgdColor //bgd input
 */

function changeBackground(selector, inputBgdColor){
    const element = document.querySelector(selector);
    const color = inputBgdColor.value; 
    element.style.backgroundColor = color;
}


/**
 * @description 
 * changeBorderColor set the background of a collection of elements tags 
 * with the value from inputBorderColor HTMLElement 
 * @param {NodeList} selector 
 * @param {HTMLInputElement} inputBorderColor //border color input
 */
function changeBorderColor(selector, inputBorderColor){
  const elements = document.querySelectorAll(selector);
  console.log(elements);
  const color = inputBorderColor.value; 
  for (let i = 0; i < elements.length; i++){
    elements[i].style.borderColor = color;
  }
}



/**
 * @description 
 * changeBorderWidth set the border width (in px) of a collection of elements tags 
 * with the value from inputBorderWidth HTMLElement 
 * @param {NodeList} selector
 * @param {HTMLInputElement} inputBorderWidthPx //border width input
 */
function changeBorderWidth(selector, inputBorderWidthPx){
  const elements = document.querySelectorAll(selector);
  const width = inputBorderWidthPx.value+"px"; 
  console.log(width);
  for (let i = 0; i < elements.length; i++){
    console.log(elements[i]); 
    elements[i].style.borderWidth = width;
  }
  console.log("border width changed"); 
}


 
 /**
 * @param {HTMLElement} tag 
 * @param {object} properities 
 * @returns {HTMLElement} / return the created element
 */

 /**
 *  @description
 * createMyElement - 
 * . create a dynamic HTMLElement, set its properties
 * . and attach it to the parent (HTMLElement)
 * . The properties (rest parameter- check labs/notes): 
 *      . must have at least id, type,
 *      . optionally text content, value, class name, min, max 
  * 
  * @param {HTMLElement} tag 
  * @param {HTMLElement} parentSelector 
  * @param  {...any} properities 
 * @returns {HTMLElement} / return the created element
  */

 function createMyElement(tag, parentSelector, ...properties) {
    // Create the new HTML element
    const element = document.createElement(tag);
    const parent = document.querySelector(parentSelector); 
  
    // Iterate over the properties and set them as attributes using setAttribute
    for (let i = 0; i < properties.length; i += 2) {
      const propName = properties[i];
      const propValue = properties[i + 1];
  
      // Set the attribute on the element
      element.setAttribute(propName, propValue);
    }
  
    // Attach the new element to the parent
    parent.append(element);
  
    // Return the created element
    return element;
  }

