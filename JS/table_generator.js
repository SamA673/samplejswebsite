// must use strict
'use strict';

// step1- 
/**
 * @description -generateTable(rowCount, colCount)
 * generateTable creates a dynamic table with exactly rowCount rows and colCount columns.
 * while loping to create the dynamic cells, you can prepare  the table html code (string) 
 * to append it to section #table-html-space (through a text editor).
 *
 *  Note - Mind the spaces and new lines to format your html output
 * before generating a new table, you should remove an old one if it exists.  
 *
 * advice: you can write this in the console and conduct some tests
 * e.g., generateTable(2,3), generateTable(3, 4)
 * 
 * @param {Number} rowCount 
 * @param {Number} colCount 
 */

function generateTable(rowCount, colCount) {
    // Check if an old table exists and remove it
    const oldTable = document.getElementById('dynamic-table');
    const oldCode = document.getElementById('codeTextArea'); 
    if (oldTable) {
      oldTable.remove();
    }
    if (oldCode){
        oldCode.remove(); 
    }
  
    // Create a new table element
    const table = document.createElement('table');
    table.id = 'dynamic-table';
    let tableCode = '\n' + "     <table>" + '\n'; 

    // Create a textArea for my code
    const textArea = document.createElement('textArea'); 
    textArea.id = "codeTextArea";
    textArea.setAttribute("cols", 25); 
    textArea.setAttribute("rows", 15);
  

    // Loop to create rows and columns
    for (let i = 0; i < rowCount; i++) {
      let row = document.createElement('tr');
      tableCode += "          <tr>" + '\n'; 
      for (let j = 0; j < colCount; j++) {
        let cell = document.createElement('td');
        cell.innerText = "Data " + (i+1) + (j+1);
        tableCode += "               <td>   "  + cell.innerText +  "   <td>          " + '\n';
        row.append(cell);
      }
      table.append(row);
      tableCode+= "         </tr>" + '\n';

    }
    tableCode+= "     </table>"; 

    // Put tableCode text into textArea 
    textArea.innerHTML = tableCode; 
  
    // Append the table and the code...
    const tableRenderSpace = document.getElementById("table-render-space");
    const tableCodeSpace = document.getElementById("table-html-space"); 
    if (tableRenderSpace && tableCodeSpace) {
      tableRenderSpace.append(table);
      tableCodeSpace.append(textArea); 
    } else {
      console.log('Uh oh.... #table-render-space or #table-html-space was not found...');
    }
  }


/** step2-
 * @description
 * createFormInputTags- 
 * - creates dynamically the SEVEN input HTMLElements (need to call `createMyElement` defined in utilities) 
 *   for each input element:
 *       - set its attributes: id,  type (number or color), default value,
 *       - attach it to a fieldset (with legend), whihc you create on the fly as well.
 *       - register an event listener every time the value in the element has changed (or you can use blur as well)
 * -  for instance:
 *          . row-count Element has to (register) listen to handler `generateTable` defined above.
 *          . col-count Element has to (register) listen to handler `generateTable` defined above.
 *          
 *          . text-color Element has to (register) listen to handler `changeTextColor` defined in utilities
 *          . table-width(%) Element has to (register) listen to handler `changeTagWidth` defined in utilities
 *          . border-width(px) Element has to (register) listen to handler `changeBorderWidth` defined in utilities
 *          ...
*/
function createFormInputTags(){
    // Select form
    const form = document.getElementById("formID");

    // Creating upper and lower section...
    createMyElement("section", "form", "id", "s1");
    createMyElement("section", "form", "id", "s2");

    // RowCount
    createMyElement("fieldset", "#s1", "id", "rCount");
    let s1l1 = createMyElement("legend", "#rCount"); 
    s1l1.innerText = "row-count";
    createMyElement("input", "#rCount", "type", "number", "value", "2"); 
 
    //ColCount
    createMyElement("fieldset", "#s1", "id", "cCount");
    let s1l2 = createMyElement("legend", "#cCount"); 
    s1l2.innerText = "col-count";
    createMyElement("input", "#cCount", "type", "number", "value", "2"); 

   // Table Width
    createMyElement("fieldset", "#s1", "id", "tWidth");
    let s1l3 = createMyElement("legend", "#tWidth"); 
    s1l3.innerText = "table-width(%)";
    createMyElement("input", "#tWidth", "type", "number", "min", "0", "max", "100", "value", "100");

    // Border Width
    createMyElement("fieldset", "#s1", "id", "bWidth");
    let s1l4 = createMyElement("legend", "#bWidth"); 
    s1l4.innerText = "border-width(px)"; 
    createMyElement("input", "#bWidth", "type", "number", "value", "1"); 

    // S2 

    // Text Color
    createMyElement("fieldset", "#s2", "id", "tColor");
    let s2l1 = createMyElement("legend", "#tColor"); 
    s2l1.innerText = "text-color"; 
    createMyElement("input", "#tColor", "type", "color", "value", "black"); 


    // Background color
    createMyElement("fieldset", "#s2", "id", "bColor");
    let s2l2 = createMyElement("legend", "#bColor"); 
    s2l2.innerText = "background-color"; 
    createMyElement("input", "#bColor", "type", "color", "value", "#FFFFFF"); 


    // Border Color
    createMyElement("fieldset", "#s2", "id", "cbColor");
    let s2l3 = createMyElement("legend", "#cbColor"); 
    s2l3.innerText = "(cell) border-color"; 
    createMyElement("input", "#cbColor", "type", "color", "value", "black"); 


    // Event Handling
    const rowCountInput = document.querySelector("#rCount > input");
    const colCountInput = document.querySelector("#cCount > input");
    const tagWidthInput = document.querySelector("#tWidth > input");
    const borderWidthInput = document.querySelector("#bWidth > input");

    const textColorInput = document.querySelector("#tColor > input");
    const backgroundColorInput = document.querySelector("#bColor > input");
    const borderColorInput = document.querySelector("#cbColor > input");

    // Column
    rowCountInput.addEventListener('blur', function(){
        generateTable(rowCountInput.value, colCountInput.value);
    });

    // Row
    colCountInput.addEventListener('blur', function(){
        generateTable(rowCountInput.value, colCountInput.value);
    });

    // Tag width
    tagWidthInput.addEventListener('blur', function(){
        changeTagWidth("#dynamic-table", tagWidthInput); 
    });

    // Border Width
    borderWidthInput.addEventListener('blur', function(){
      changeBorderWidth("#dynamic-table", borderWidthInput); 
  });


    // Text Color
    textColorInput.addEventListener('blur', function(){
      changeTextColor("#dynamic-table", textColorInput); 
  });

      // Background color
      backgroundColorInput.addEventListener('blur', function(){
        changeBackground("#dynamic-table", backgroundColorInput); 
    });


    borderColorInput.addEventListener('blur', function(){
      changeBorderColor("td", borderColorInput); 
  });


}
   



// step 3
// NOTE 
//    - The whole code must run once the HTML code has been parsed (into a DOM tree).
//    - You are not allowed to use defer attribute in the page.html File.
//    - All JS files must be coded inside the <head> section of page.html

// Define a function called  mainProgram as the entry point of the application execution.
// In other words:
//    . is run only when the special event (DOM content) has finished loading
//    .  calls createFormInputTags

//    Note - you can use anonymous function instead of mainProg if you want.

    function mainProgram() {
        createFormInputTags();
        generateTable(2,2); 
      }
      
      // Use the DOMContentLoaded event to ensure the code runs after the DOM is ready
      document.addEventListener('DOMContentLoaded', mainProgram);



